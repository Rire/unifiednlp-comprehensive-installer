# Magisk UnifiedNlp Overlay Comprehensive Installer

Install UnifiedNlp alongside GAPPS with this Magisk Module
* Tested on LineageOS 18 

This is based off the Magisk module written by jamerst, and ysc3839, using files from greenflash1986's UnifiedNlpOverlay repository.

* https://github.com/jamerst/magisk-intense-night-light
* https://github.com/ysc3839/magisk-permissionreviewenabler


# Usage

Simply download the zip file from this repository and run. If any errors pop up, refer to the below repository for manual installation. 

**Do not use this without gapps, you'll fail to boot and enter a bootloop.** Refer to this guide if you want UnifiedNlp without gapps, since I'm told this works: https://blog.eowyn.net/unifiednlp/#Install

* https://github.com/greenflash1986/UnifiedNlpOverlay
